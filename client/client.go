package client

import (
	"io"
	"log"
	"net"

	"../protocol"
)

type GraffitiClient struct {
	conn          net.Conn
	commandReader *protocol.CommandReader
	commandWriter *protocol.CommandWriter
	name          string
	incoming      chan protocol.MessageCommand
}

func NewGraffitiClient() *GraffitiClient {
	return &GraffitiClient{incoming: make(chan protocol.MessageCommand)}
}

func (gc *GraffitiClient) Close() {
	gc.conn.Close()
}

func (gc *GraffitiClient) Dial(address string) error {
	conn, err := net.Dial("tcp", address)
	if err == nil {
		gc.conn = conn
	}

	gc.commandReader = protocol.NewCommandReader(conn)
	gc.commandWriter = protocol.NewCommandWriter(conn)

	return err
}

func (gc *GraffitiClient) Incoming() chan protocol.MessageCommand {
	return gc.incoming
}

func (gc *GraffitiClient) Send(command interface{}) error {
	return gc.commandWriter.Write(command)
}

func (gc *GraffitiClient) SendMessage(message string) error {
	return gc.Send(protocol.SendCommand{Message: message})
}

func (gc *GraffitiClient) SetName(name string) error {
	return gc.Send(protocol.NameCommand{Name: name})
}

func (gc *GraffitiClient) Start() {
	for {
		cmd, err := gc.commandReader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Printf("Read error %v\n", err)
		}

		if cmd != nil {
			switch v := cmd.(type) {
			case protocol.MessageCommand:
				gc.incoming <- v
			default:
				log.Printf("Unknown command: %v\n", v)
			}
		}
	}
}
