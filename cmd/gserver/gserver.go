package gserver

import "bitbucket.org/jboydt/graffiti/server"

func main() {
	gs := server.NewGraffitiServer()
	gs.Listen(":3333")
	gs.Start()
}
