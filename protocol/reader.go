package protocol

import (
	"bufio"
	"io"
	"log"
)

type CommandReader struct {
	reader *bufio.Reader
}

func NewCommandReader(reader io.Reader) *CommandReader {
	return &CommandReader{reader: bufio.NewReader(reader)}
}

func (cr *CommandReader) Read() (interface{}, error) {
	commandName, err := cr.reader.ReadString(' ')
	if err != nil {
		return nil, err
	}

	switch commandName {
	case "MESSAGE ":
		user, err := cr.reader.ReadString(' ')
		if err != nil {
			return nil, err
		}

		message, err := cr.reader.ReadString('\n')
		if err != nil {
			return nil, err
		}

		return MessageCommand{Name: user[:len(user)-1], Message: message[:len(message)-1]}, nil

	default:
		log.Printf("Unknown command: %v\n", commandName)
	}

	return nil, UnknownCommand
}
