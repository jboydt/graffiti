package protocol

import (
	"fmt"
	"io"
)

type CommandWriter struct {
	writer io.Writer
}

func NewCommandWriter(writer io.Writer) *CommandWriter {
	return &CommandWriter{writer: writer}
}

func (cw *CommandWriter) writeString(msg string) error {
	_, err := cw.writer.Write([]byte(msg))

	return err
}

func (cw *CommandWriter) Write(command interface{}) error {
	var err error

	switch v := command.(type) {
	case SendCommand:
		err = cw.writeString(fmt.Sprintf("SEND %v\n", v.Message))
	case MessageCommand:
		err = cw.writeString(fmt.Sprintf("MESSAGE %v %v\n", v.Name, v.Message))
	case NameCommand:
		err = cw.writeString(fmt.Sprintf("NAME %v\n", v.Name))
	default:
		err = UnknownCommand
	}

	return err
}
