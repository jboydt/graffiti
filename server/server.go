package server

import (
	"io"
	"log"
	"net"
	"sync"

	"bitbucket.org/jboydt/graffiti/protocol"
)

type GraffitiServer struct {
	listener net.Listener
	clients  []*GraffitiClient
	mutex    *sync.Mutex
}

type GraffitiClient struct {
	conn   net.Conn
	name   string
	writer *protocol.CommandWriter
}

func NewGraffitiServer() *GraffitiServer {
	return &GraffitiServer{mutex: &sync.Mutex{}}
}

func (gs *GraffitiServer) Listen(address string) error {
	listener, err := net.Listen("tcp", address)
	if err == nil {
		gs.listener = listener
	}

	log.Printf("Listening on %v\n", address)

	return err
}

func (gs *GraffitiServer) Broadcast(command interface{}) error {
	for _, client := range gs.clients {
		client.writer.Write(command)
	}

	return nil
}

func (gs *GraffitiServer) Start() {
	for {
		conn, err := gs.listener.Accept()
		if err != nil {
			log.Printf("%v\n", err)
		} else {
			client := gs.accept(conn)
			go gs.serve(client)
		}
	}
}

func (gs *GraffitiServer) Close() {
	gs.listener.Close()
}

func (gs *GraffitiServer) accept(conn net.Conn) *GraffitiClient {
	log.Printf("Accepting connection from %v, total clients: %v\n",
		conn.RemoteAddr().String(), len(gs.clients)+1)

	gs.mutex.Lock()
	defer gs.mutex.Unlock()

	client := &GraffitiClient{conn: conn, writer: protocol.NewCommandWriter(conn)}
	gs.clients = append(gs.clients, client)

	return client
}

func (gs *GraffitiServer) remove(client *GraffitiClient) {
	gs.mutex.Lock()
	defer gs.mutex.Unlock()

	for i, check := range gs.clients {
		if check == client {
			gs.clients = append(gs.clients[:i], gs.clients[i+1:]...)
		}
	}

	log.Printf("Closing connection from %v\n", client.conn.RemoteAddr().String())
	client.conn.Close()
}

func (gs *GraffitiServer) serve(client *GraffitiClient) {
	commandReader := protocol.NewCommandReader(client.conn)
	defer gs.remove(client)

	for {
		cmd, err := commandReader.Read()
		if err != nil && err != io.EOF {
			log.Printf("Read error: %v\n", err)
		}

		if cmd != nil {
			switch v := cmd.(type) {
			case protocol.SendCommand:
				go gs.Broadcast(protocol.MessageCommand{Message: v.Message, Name: client.name})
			case protocol.NameCommand:
				client.name = v.Name
			}
		}

		if err == io.EOF {
			break
		}
	}
}
